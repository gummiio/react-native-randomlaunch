# Randomlaunch

An app that generate a completely random website from search engine every 4 hours.

#### - Loading Screen
![Screenshot 1](screenshots/1.jpg)

#### - On boarding steps
![Screenshot 3](screenshots/3.jpg)

#### - App Main screen
![Screenshot 2](screenshots/2.jpg)
