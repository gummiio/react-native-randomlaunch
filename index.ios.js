/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

import RandomLaunch from './Randomlaunch/Randomlaunch';

class RandomLaunchApp extends Component {
    render() {
        return (
            <RandomLaunch />
        );
    }
}

AppRegistry.registerComponent('RandomLaunch', () => RandomLaunchApp);
