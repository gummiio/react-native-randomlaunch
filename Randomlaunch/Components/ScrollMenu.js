
import React, { Component } from 'react';

import {
    StyleSheet,
    View,
    ScrollView,
    Text,
    TouchableHighlight
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

class ScrollMenu extends Component {

    constructor(props) {
        super(props);

        this.state = {
            currentIndex: 0,
            hasPrev: false,
            hasNext: props.items.length > 1
        }

        this._onScroll = this._onScroll.bind(this);
        this._prevCategory = this._prevCategory.bind(this);
        this._nextCategory = this._nextCategory.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.items != this.props.items) {
            this.refs.scrollView.scrollTo({x: 0, animated: false});
        }
    }

    _menuItem(item, key) {
        return (
            <View key={key} style={styles.scrollItem}>
                <Text style={styles.itemText}>{item}</Text>
            </View>
        );
    }

    _onScroll(scrollView) {
        let currentIndex = Math.floor(scrollView.nativeEvent.contentOffset.x / 160);
        let hasPrev = currentIndex !== 0;
        let hasNext = currentIndex !== this.props.items.length - 1;

        this.setState({currentIndex, hasPrev, hasNext});

        if (typeof this.props.onItemSelected === 'function') {
            return this.props.onItemSelected(currentIndex);
        }
    }

    _prevCategory() {
        let scrollView = this.refs.scrollView;
        let currentX = this.state.currentIndex * 160;
        let newX = currentX - 160;
        let min = 0;

        if (newX < min) newX = min;

        this.refs.scrollView.scrollTo({x: newX});
    }

    _nextCategory() {
        let scrollView = this.refs.scrollView;
        let currentX = this.state.currentIndex * 160;
        let newX = currentX + 160;
        let max = 160 * (this.props.items.length - 1);

        if (newX >= max) newX = max;

        this.refs.scrollView.scrollTo({x: newX});
    }

    _getPreText() {
        if (this.props.preText) {
            return this.props.preText();
        }

        return this.props.showPreText?
            <Text style={styles.preText}>Category:</Text> :
            <Text />;
    }

    render() {
        return (
            <View style={styles.container}>
                {this._getPreText()}
                <View style={styles.scrollHolder}>
                    <TouchableHighlight
                        style={[
                            styles.navigation,
                            {
                                opacity: this.state.hasPrev? 1 : 0
                            }
                        ]}
                        onPress={() => this._prevCategory()}
                    >
                        <Icon style={styles.icon} name="angle-left" size={32} />
                    </TouchableHighlight>

                    <ScrollView
                        ref="scrollView"
                        style={styles.ScrollMenu}
                        horizontal={true}
                        pagingEnabled={true}
                        showsHorizontalScrollIndicator={false}
                        onScroll={this._onScroll}
                        scrollEventThrottle={1000}
                    >
                        {this.props.items.map(this._menuItem)}
                    </ScrollView>

                    <TouchableHighlight
                        style={[
                            styles.navigation,
                            {
                                opacity: this.state.hasNext? 1 : 0
                            }
                        ]}
                        onPress={() => this._nextCategory()}
                    >
                        <Icon style={styles.icon} name="angle-right" size={32} />
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 15
    },
    preText: {
        fontSize: 12,
        textAlign: "left",
        color: "#c3c3c3",
        marginLeft: 30,
        position: "relative",
        top: 2
    },
    scrollHolder: {
        flexDirection: 'row',
        width: 220,
        alignItems: 'center',
    },
    navigation: {
        paddingTop: 5,
        paddingBottom: 5,
        width: 30,
    },
    icon: {
        textAlign: "center",
        color: '#c3c3c3'
    },
    ScrollMenu: {
        // height: 30,
        width: 160,
        // backgroundColor: "#130e0e",
        borderRadius: 5,
    },
    scrollItem: {
        width: 160,
        padding: 10,
    },
    itemText: {
        textAlign: 'center',
        color: '#c3c3c3',
        fontWeight: "500",
        fontSize: 18,
        letterSpacing: 1.5
    },
});

module.exports = ScrollMenu;
