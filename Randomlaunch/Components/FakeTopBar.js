
import React, { Component } from 'react';

import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity
} from 'react-native';

class FakeTopBar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            leftText: props.backPosition == 'left'? '« Back' : '',
            rightText: props.backPosition == 'right'? 'Back »' : ''
        }

        this._goBack = this._goBack.bind(this);
    }

    _goBack(position) {
        if (position == 'right' && this.state.rightText) {
            this.props.navigator.jumpBack();
        }

        if (position == 'left' && this.state.leftText) {
            this.props.navigator.jumpBack();
        }
    }

    render() {
        return (
            <View style={[styles.FakeTopBar, this.props.style || {}]}>
                <TouchableOpacity style={styles.action} onPress={() => this._goBack('left')}>
                    <Text style={styles.actionText}>{this.state.leftText}</Text>
                </TouchableOpacity>
                <Text style={styles.barTitle}>{this.props.title}</Text>
                <TouchableOpacity style={styles.action} onPress={() => this._goBack('right')}>
                    <Text style={styles.actionText}>{this.state.rightText}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    FakeTopBar: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        backgroundColor: '#130e0e',
        height: 60,
        borderBottomWidth: 3,
        borderBottomColor: "red",
    },
    barTitle: {
        flex: 1,
        textAlign: "center",
        color: '#c3c3c3',
        fontWeight: "700",
        letterSpacing: .5
    },
    action: {
        width: 70,
        paddingRight: 10,
        paddingLeft: 10
    },
    actionText: {
        color: '#c3c3c3',
        fontStyle: "italic"
    }
});

module.exports = FakeTopBar;
