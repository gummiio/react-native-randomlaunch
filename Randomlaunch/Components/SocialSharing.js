/**
 * Status Bar Placeholder
 */

import React, { Component } from 'react';

import {
    StyleSheet,
    View,
    Text,
    TouchableHighlight
} from 'react-native';

import Share from 'react-native-share';
import Icon from 'react-native-vector-icons/FontAwesome';

class SocialSharing extends Component {

    constructor(props) {
        super(props);
    }


    _onShare() {
        Share.open({
            share_text: "RandomLaunch - exploration of new random web sites",
            share_URL: "https://randomlaunch.com",
            title: "RandomLaunch - exploration of new random web sites"
        },(e) => {
            console.log(e);
        });
    }

    render() {
        return (
            <View style={styles.buttonHolder}>
                <Icon.Button name="bullhorn" style={styles.button} color="#c3c3c3" backgroundColor="transparent" onPress={this._onShare}>
                     <Text style={styles.buttonText}>Share RandomLaunch</Text>
                </Icon.Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    buttonHolder: {
        margin: 15,
        marginTop: 20
    },
    button: {
        backgroundColor: '#130e0e',
        padding: 10,
        paddingRight: 35,
        paddingLeft: 35,
        borderColor: '#dc1623',
        borderBottomWidth: 2
    },
    buttonText: {
        color: '#c3c3c3',
        fontSize: 14,
        fontWeight: '700',
        letterSpacing: 1.5
    }
});

module.exports = SocialSharing;
