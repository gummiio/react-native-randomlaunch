/**
 * Status Bar Placeholder
 */

import React, { Component } from 'react';

import {
    ActivityIndicator,
    StyleSheet,
    View,
    Text
} from 'react-native';

import {AnimatedCircularProgress} from 'react-native-circular-progress';

class CountdownCircle extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            interval: props.interval || 1000,
            decrement: props.decrement || 1,
            countDown: props.countDown || -1,
            timeFrame: props.timeFrame || 4 * 60 * 60,
            countDownInterval: null,
            fill: props.fill || 0,
            displayHr: '--',
            displayMin: '--',
            displaySec: '--'
        };

        this._countDown = this._countDown.bind(this);
        this._getStyle = this._getStyle.bind(this);
        this._insideCircle = this._insideCircle.bind(this);
    }

    componentDidMount() {
        this._restartCountDown();
    }

    componentWillUnmount() {
        this._maybeClearInterval();
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.noListener) return;

        if (nextProps.countDown != this.state.countDown) {
            this.setState({
                countDown: nextProps.countDown
            });

            this._restartCountDown();
        }
    }

    _restartCountDown() {
        this._maybeClearInterval();

        let countDownInterval = setInterval(this._countDown, this.state.interval);

        this.setState({
            countDownInterval,
            // isLoading: false
        });
    }

    _maybeClearInterval() {
        if (this.state.countDownInterval) {
            clearInterval(this.state.countDownInterval);

            this.setState({
                countDownInterval: null
            });
        }

        if (! this.props.noListener) {
            this.setState({isLoading: true});
        }
    }

    _countDown() {
        let countDown = this.state.countDown;

        if (countDown === -1) {
            return this._countDownDisabled();
        }

        if (countDown <= 0) {
            return this._countDownReachedZero();
        }

        if (countDown > 0 && this.state.isLoading) {
            this.setState({isLoading: false});
        }

        countDown -= this.state.decrement;

        this.setState({countDown});

        this._setTimerDisplay();
    }

    _setTimerDisplay() {
        let sec, min, hr, fill;
        sec = min = hr = fill = 0;

        sec = this.state.countDown;

        let timeFrame = this.state.timeFrame;
        fill = Math.round((timeFrame - sec) / timeFrame * 10000) / 100;

        if (sec > 60) {
            min = Math.floor(sec / 60);
            sec = sec - min * 60;
        }

        if (min > 60) {
            hr = Math.floor(min / 60);
            min = min - hr * 60;
        }

        this.setState({
            fill,
            displayHr: String('00' + hr).slice(-2),
            displayMin: String('00' + min).slice(-2),
            displaySec: String('00' + sec).slice(-2)
        });
    }

    _countDownDisabled() {
        this._maybeClearInterval();
        this.setState({
            isLoading: true,
            displayHr: '--',
            displayMin: '--',
            displaySec: '--'
        })
    }

    _countDownReachedZero() {
        this._maybeClearInterval();

        if (typeof this.props.countDownReachedZero === 'function') {
            this.props.countDownReachedZero(this);
        }
    }

    _insideCircle(fill) {
        if (typeof this.props.insideCircle === 'function') {
            return this.props.insideCircle(fill, this);
        }

        return this._countDownDisplay(fill);
    }

    _countDownDisplay(fill) {
        let notes = <Text />;
        let timer = <Text />;

        if (this._getProps('showNotes', true)) {
            if (this.state.isLoading) {
                notes = (
                    <Text style={this._getStyle('timerNotes')}>
                        reaching out to space...
                    </Text>
                );
            } else {
                notes = (
                    <Text style={this._getStyle('timerNotes')}>
                        time until the next random link
                    </Text>
                );
            }
        }

        if (this.state.isLoading) {
            timer = (
                <ActivityIndicator animating={true} size="large" />
            );
        } else {
            timer = (
                <Text style={this._getStyle('timer')}>
                    <Text style={this._getStyle('timerHr')}>{this.state.displayHr}:</Text>
                    <Text style={this._getStyle('timerMin')}>{this.state.displayMin}:</Text>
                    <Text style={this._getStyle('timerSec')}>{this.state.displaySec}</Text>
                </Text>
            );
        }

        return (
            <View style={this._getStyle('timerHolder')}>
                {timer}

                {notes}
            </View>
        );
    }

    // extends the stylesheet if passed by prop
    _getStyle(selector) {
        return [styles[selector], this.props[selector] || {}];
    }

    _getProps(prop, def) {
        return typeof this.props[prop] === 'undefined'?
            def : this.props[prop];
    }

    render() {
        return (
            <View style={this._getStyle('circleHolder')}>
                <AnimatedCircularProgress
                    ref='circularProgress'
                    size={this._getProps('circleSize', 240)}
                    width={this._getProps('circleWidth', 8)}
                    fill={this.state.fill}
                    prefill={this._getProps('circlePreFill', 0)}
                    tintColor={this._getProps('circleTintColor', '#dc1623')}
                    backgroundColor={this._getProps('circleBackgroundColor', '#130e0e')}
                    rotation={this._getProps('circleRotation', 180)}
                >
                    {this._insideCircle}
                </AnimatedCircularProgress>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    circleHolder: {
        position: "relative",
        height: 240,
        width: 240,
        marginBottom: 30,
        borderRadius: 120,
        shadowColor: "white",
        shadowOpacity: 0.15,
        shadowRadius: 1,
        shadowOffset: {
            height: 0,
            width: 0
        },
    },
    timerHolder: {
        position: "absolute",
        backgroundColor: "transparent",
        top: 90,
        left: 0,
        width: 240
    },
    timer: {
        fontSize: 42,
        color: 'white',
        textAlign: 'center',
        fontWeight: "100",
        color: '#c3c3c3'
    },
    timerHr: {

    },
    timerMin: {

    },
    timerSec: {

    },
    timerNotes: {
        color: "#c3c3c3",
        fontSize: 10,
        marginTop: 10,
        fontWeight: "300",
        fontStyle: 'italic',
        textAlign: 'center',
    },
});

module.exports = CountdownCircle;
