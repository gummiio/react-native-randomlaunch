/**
 * Status Bar Placeholder
 */

import React, { Component } from 'react';

import {
    StyleSheet,
    View,
    StatusBar
} from 'react-native';

class StatusBarHolder extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[styles.StatusBarHolder, this.props.style || {}]}>
                <StatusBar hidden={false} showHideTransition="slide" />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    StatusBarHolder: {
        backgroundColor: 'transparent',
        height: 20
    }
});

module.exports = StatusBarHolder;
