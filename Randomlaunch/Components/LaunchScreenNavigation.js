
import React, { Component } from 'react';

import {
    StyleSheet,
    View,
    Dimensions,
    TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

class LaunchScreenNavigation extends Component {

    constructor(props) {
        super(props);

        this.state = {
            activePage: 'launch'
        };

        this._buttonStyle = this._buttonStyle.bind(this);
        this._buttonColor = this._buttonColor.bind(this);
    }

    _buttonStyle(button, extra = {}) {
        let style = [styles.button, extra];
        if (this.state.activePage == button) {
            style.push(styles.active);
        }

        return style;
    }

    _buttonColor(button) {
        if (this.state.activePage == button) {
            return '#dc1623';
        }

        return '#c3c3c3';
    }

    render() {
        return (
            <View style={styles.buttonSet}>
                <TouchableOpacity
                    style={this._buttonStyle('about')}
                    onPress={() => {
                        this.props.googleAnalytic.event('Buttons', 'Clicked', 'About Button', '');
                        this.props.onNavigatorClicked('about')
                    }}
                >
                    <Icon style={styles.icon} name="question" size={22} color={this._buttonColor('about')} />
                </TouchableOpacity>

                {
                    // <TouchableOpacity
                    //     style={this._buttonStyle('launch', styles.middle)}
                    //     onPress={() => this.props.onNavigatorClicked('launch')}
                    // >
                    //     <Icon style={styles.icon} name="rocket" size={30} color={this._buttonColor('launch')} />
                    // </TouchableOpacity>
                }

                <TouchableOpacity
                    style={this._buttonStyle('setting')}
                    onPress={() => {
                        this.props.googleAnalytic.event('Buttons', 'Clicked', 'Setting Button', '');
                        this.props.onNavigatorClicked('setting')
                    }}
                >
                    <Icon style={styles.icon} name="cog" size={22} color={this._buttonColor('setting')} />
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    buttonSet: {
        height: 80,
        width: Dimensions.get('window').width,
        flexDirection: "row",
        justifyContent: "flex-end",
        // alignItems: 'center',
        // marginTop: 5,
        marginBottom: 10,
        paddingRight: 25,
        paddingLeft: 25,
        // backgroundColor: 'red'
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 50,
        height: 50,
        borderRadius: 25,
        // borderWidth: 5,
        // borderColor: '#130e0e',
        // borderStyle: "solid",
        marginLeft: 10,
        backgroundColor: '#130e0e',
        shadowColor: "white",
        shadowOpacity: 0.1,
        shadowRadius: 1,
        shadowOffset: {
            height: 0,
            width: 0
        },
    },
    middle: {
        width: 80,
        height: 80,
        borderRadius: 40,
        top: 10,
    },
    active: {
        borderColor: '#dc1623',
    },
    icon: {
        // left: -1,
    }
});

module.exports = LaunchScreenNavigation;
