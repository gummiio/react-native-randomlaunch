/**
 * Launch Button
 */

import React, { Component } from 'react';

import {
    StyleSheet,
    View,
    Text
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import Browser from 'react-native-browser';

class LaunchButton extends Component {
    constructor(props) {
        super(props);

        this._buttonPressed = this._buttonPressed.bind(this);
    }

    _buttonPressed() {
        let needToOpen = true;

        if (typeof this.props.callback === 'function') {
            needToOpen = this.props.callback();
        }

        if (needToOpen) {
            Browser.open(this.props.webUrl, {
                showUrlWhileLoading: false,
                navigationButtonsHidden: false,
                showActionButton: true,
                showDoneButton: true,
                doneButtonTitle: 'Close',
                showPageTitles: true,
                disableContextualPopupMenu: false,
                hideWebViewBoundaries: false
            });

            if (this.props.googleAnalytic) {
                this.props.googleAnalytic.event('Buttons', 'clicked', 'Launch Button', '');
            }
        }
    }

    onComponentWillUnmount() {
        Browser.close();
    }

    render() {
        return (
            <View style={styles.buttonHolder}>
                <Icon.Button name="rocket" style={styles.button} color="#c3c3c3" backgroundColor="transparent" onPress={this._buttonPressed}>
                    <Text style={styles.buttonText}>LAUNCH</Text>
                </Icon.Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    buttonHolder: {
        marginTop: 30,
        marginBottom: 10,
    },
    button: {
        backgroundColor: '#130e0e',
        padding: 10,
        paddingRight: 35,
        paddingLeft: 35,
        borderColor: '#dc1623',
        borderBottomWidth: 2
    },
    buttonText: {
        color: '#c3c3c3',
        fontSize: 14,
        fontWeight: '700',
        letterSpacing: 1.5
    }
});

module.exports = LaunchButton;
