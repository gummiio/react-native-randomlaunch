/**
 * Randomlaunch App Entry Point
 */

import React, { Component } from 'react';

import {
    Text,
    View,
    Image,
    AsyncStorage,
    Navigator
} from 'react-native';

import SettingControl from './Libraries/SettingControl';
import GoogleAnalytic from './Libraries/GoogleAnalytic';
import StatusBarHolder from './Components/StatusBarHolder';
import MainScreen from './Pages/MainScreen';
import Tutorials from './Pages/Tutorials/Tutorials';

class RandomLaunch extends Component {

    constructor(props) {
        super(props);

        this.state = {
            tutorialFinished: null,
            tutorialStep: 1
        }

        this.settingControl = new SettingControl({
            tutorialFinished: false,
            tutorialStep: 1,
            notificationEnabled: false,
            authKey: ''
        });

        this.googleAnalytic = new GoogleAnalytic('UA-80283472-1');

        this._renderScene = this._renderScene.bind(this);
        this._onSettingLoaded = this._onSettingLoaded.bind(this);
        this._onSettingUpdated = this._onSettingUpdated.bind(this);
    }

    componentWillMount() {
        // this.settingControl.clear();
        this.settingControl.addEventListener('settingLoaded', this._onSettingLoaded);
        this.settingControl.addEventListener('settingUpdated', this._onSettingUpdated);

        this.settingControl.loadSetting();
    }

    componentWillUnmount() {
        this.settingControl.removeEventListener('settingLoaded', this._onSettingLoaded);
        this.settingControl.removeEventListener('settingUpdated', this._onSettingUpdated);
    }

    _onSettingLoaded(event) {
        this.setState(event.settings);

        this.refs.mainNavigator.resetTo({
            tutorialFinished: this.state.tutorialFinished
        });
    }

    _onSettingUpdated(event) {
        let {key, value, oldValue} = event;
        console.log(`updated from top level: ${key} updated to ${value}`);
    }

    _renderScene(route, navigator) {
        let globalProps = {
            mainNavigator: navigator,
            settingControl: this.settingControl,
            googleAnalytic: this.googleAnalytic
        };

        switch (route.tutorialFinished) {
            case true:
                return <MainScreen {...globalProps} />;

            case false:
                return <Tutorials step={this.state.tutorialStep} {...globalProps} />;

            default:
                return <Image source={require('image!background')} style={{flex: 1, width: null, height: null}} />;
        }
    }

    render() {
        return (
            <Navigator
                initialRoute={{tutorialFinished: this.state.tutorialFinished}}
                ref="mainNavigator"
                renderScene={this._renderScene}
            />
        );
    }
}

module.exports = RandomLaunch;
