import DeviceInfo from 'react-native-device-info';

class RandomlaunchApi {
    constructor(apiKey = '', Auth = '') {
        this.requestUrl = 'https://randomlaunch.com/api/v1/';
        this.headers = {
            'randomlaunch-api' : apiKey,
            'Authorization' : 'Basic ' + Auth
        };
    }

    authKey(key) {
        this.headers.Authorization = 'Basic ' + key;

        return this;
    }

    login(Auth) {
        return this._post('login');
    }

    register() {
        let params = {
            device_unique_id: DeviceInfo.getUniqueID(),
            device_manufacturer: DeviceInfo.getManufacturer(),
            device_model: DeviceInfo.getModel(),
            device_id: DeviceInfo.getDeviceId(),
            device_name: DeviceInfo.getDeviceName(),
            device_locale: DeviceInfo.getDeviceLocale(),
            system_name: DeviceInfo.getSystemName(),
            system_version: DeviceInfo.getSystemVersion()
        };

        return this._post('register', params);
    }

    getPage() {

    }

    getLaunchUrl(category = '') {
        if (category) {
            category = '/' + category;
        }

        return 'https://randomlaunch.com/launch'+category;
    }

    getCountDown() {
        return this._get('launch/countdown');
    }

    getCategories() {
        return this._get('categories');
    }

    _get(endpoint, params: {}) {
        return fetch(this.requestUrl + endpoint,
            Object.assign({method: "GET"}, {headers: this.headers}, params)
        );
    }

    _post(endpoint, params: {}) {
        let formData  = new FormData();

        for(key in params) {
            formData.append(key, params[key]);
        }

        let args =  {
            method: "POST",
            headers: this.headers,
            body: formData
        };

        return fetch(this.requestUrl + endpoint, args);
    }

}

module.exports = RandomlaunchApi;
