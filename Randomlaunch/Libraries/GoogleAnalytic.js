
import {
  Analytics,
  Hits,
  Experiment
} from 'react-native-google-analytics';

var DeviceInfo = require('react-native-device-info');

class GoogleAnalytic {
    constructor(appID) {
        let clientId = DeviceInfo.getUniqueID();
        let version = DeviceInfo.getVersion();
        let agent = DeviceInfo.getUserAgent();
        this.ga = new Analytics(appID, clientId, version, agent);
    }

    screenView(screen) {
        var screenView = new Hits.ScreenView(
            'RandomLaunch Mobile App',
            screen,
            DeviceInfo.getReadableVersion(),
            DeviceInfo.getBundleId()
        );
        this.ga.send(screenView);
    }

    event(category, action, label, value) {
        var gaEvent = new Hits.Event(category, action, label, value);
        this.ga.send(gaEvent);
    }

}

module.exports = GoogleAnalytic;
