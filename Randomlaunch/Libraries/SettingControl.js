import {
    AsyncStorage
} from 'react-native';

import EventEmitter from 'EventEmitter';

class SettingControl {
    constructor(defaults = {}) {
        this.settings = {};
        this.defaults = defaults;
        this.eventEmitter = new EventEmitter();
        this.eventHandlers = new Map();
    }

    addEventListener(name, fn) {
        let listener = this.eventEmitter.addListener(name, fn);
        this.eventHandlers.set(fn, listener);
    }

    removeEventListener(name, fn) {
        var listener = this.eventHandlers.get(fn);
        if (! listener) {
          return;
        }

        listener.remove();
        this.eventHandlers.delete(fn);
    }

    loadSetting() {
        AsyncStorage.getItem('appSettings')
            .then(data => {
                this.settings = data? JSON.parse(data) : {};
                let changed = false;

                // add the "default" to setting if not exists
                Object.keys(this.defaults).map(key => {
                    if (typeof this.settings[key] === 'undefined') {
                        this.settings[key] = this.defaults[key];
                        changed = true;
                    }
                });

                if (changed) this.save();

                this.eventEmitter.emit('settingLoaded', {settings: this.settings});
            })
            .done();

        return this;
    }

    setItem(key, value) {
        this._callUpdatedListeners(key, value, this.settings[key]);
        this.settings[key] = value;
        return this.save();
    }

    setItems(pairs) {
        Object.keys(pairs).map(key => {
            this.setItem(key, pairs[key]);
        });
    }

    getItem(key, _default) {
        return typeof this.settings[key] === 'undefined'? _default : this.settings[key];
    }

    save() {
        return AsyncStorage.setItem('appSettings', JSON.stringify(this.settings));
    }

    clear() {
        return AsyncStorage.removeItem('appSettings');
    }

    _callUpdatedListeners(key, value, oldValue) {
        this.eventEmitter.emit('settingUpdated', {key, value, oldValue});
    }

}

module.exports = SettingControl;
