/**
 * About Screen
 */

import React, { Component } from 'react';

import {
    StyleSheet,
    Text,
    View,
    Image,
    Switch,
    AppState,
    ScrollView,
    PushNotificationIOS
} from 'react-native';

import FakeTopBar from '../Components/FakeTopBar';
import SocialSharing from '../Components/SocialSharing';

class SettingScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            switchDisabled: false,
            notificationEnabled: props.settingControl.getItem('notificationEnabled')
        };

        this._onAppStateChange = this._onAppStateChange.bind(this);
        this._checkNotificationPermission = this._checkNotificationPermission.bind(this);
        this._onValueChanged = this._onValueChanged.bind(this);
        this._displayDisableNotes = this._displayDisableNotes.bind(this);
    }

    componentWillMount() {
        AppState.addEventListener('change', this._onAppStateChange);
        this._checkNotificationPermission();

        this.props.googleAnalytic.screenView('About Screen');
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this._onAppStateChange);
    }

    _onAppStateChange(currentAppState) {
        if (currentAppState == 'active') {
            this._checkNotificationPermission();
        }
    }

    _checkNotificationPermission() {
        PushNotificationIOS.checkPermissions(permissions => {
            let {alert, badge, sound} = permissions;
            if (!alert || !badge || !sound) {
                this.setState({switchDisabled: true});

                this.props.googleAnalytic.event('Switches', 'Switched', 'Notification', 0);
            } else {
                this.setState({switchDisabled: false});

                this.props.googleAnalytic.event('Switches', 'Switched', 'Notification', 1);
            }
        });
    }

    _onValueChanged(notificationEnabled) {
        this.props.settingControl.setItems({notificationEnabled});
        this.setState({notificationEnabled});
    }

    _displayDisableNotes() {
        if (this.state.switchDisabled) {
            return (
                <View style={styles.disableNotes}>
                    <Text style={styles.disableHeading}>Ooops!</Text>
                    <Text style={styles.disableText}>
                        It seems like you have notification turned off on your device. To turn it back on your device, go to
                    </Text>
                    <Text style={styles.disableInstruction}>
                        Settings > RandomLaunch > Notifications
                    </Text>
                </View>
            );
        } else {
            return (
                <View>
                    <Text style={styles.description}>
                        Do you want us to notify you when <Text style={styles.semi}>the next random link</Text> is launched?
                    </Text>

                    <Text style={[styles.description, styles.lastDescription]}>
                        Don't worry, we won't spam you with notifications. We will only schedule the next launch time until you open the app again. :)
                    </Text>
                </View>
            );
        }
    }

    render() {
        return (
            <Image source={require('image!background')} style={styles.container}>
                <FakeTopBar {...this.props} title="Setting" backPosition="left" />
                <View style={{flex:1, marginTop:30}}>
                    <Text style={styles.listHeader}>
                        NOTIFICATIONS
                    </Text>

                    {this._displayDisableNotes()}

                    <View style={[styles.settingField, this.state.switchDisabled? {opacity: .2} : {}]}>
                        <Text style={styles.settingLabel}>
                            {this.state.notificationEnabled? 'Yep, you let me know!': 'Nyet! leave me alone...'}
                        </Text>

                        <Switch
                            disabled={this.state.switchDisabled}
                            onValueChange={this._onValueChanged}
                            value={this.state.notificationEnabled}
                            onTintColor="#dc1623"
                            thumbTintColor="#c3c3c3"
                            tintColor="#c3c3c3"
                        />
                    </View>

                    <Text style={[styles.listHeader, styles.secondHeader]}>
                        TELL FRIENDS ABOUT US
                    </Text>

                    <SocialSharing />
                </View>
            </Image>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null
    },
    listHeader: {
        color: '#c3c3c3',
        paddingLeft: 15,
        // marginBottom: 10,
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 1.5
    },
    description: {
        fontSize: 12,
        lineHeight: 20,
        color: '#c3c3c3',
        padding: 15,
        paddingBottom: 0,
        // marginBottom: 10
    },
    lastDescription: {
        marginBottom: 25
    },
    semi: {
        fontWeight: '700'
    },
    settingField: {
        backgroundColor: '#130e0e',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 15
    },
    settingLabel: {
        flex: 1,
        fontSize: 14,
        fontWeight: "500",
        color: "#c3c3c3"
    },
    disableNotes: {
        padding: 15,
        marginTop: 20
    },
    disableHeading: {
        color: '#c3c3c3',
        paddingBottom: 10,
        fontWeight: '700',
        fontSize: 14
    },
    disableText: {
        color: '#c3c3c3',
        paddingBottom: 15,
        lineHeight: 22,
        fontSize: 12
    },
    disableInstruction: {
        color: '#c3c3c3',
        fontWeight: '500',
        fontStyle: 'italic',
        fontSize: 12
    },
    secondHeader: {
        marginTop: 30
    }
});

module.exports = SettingScreen;
