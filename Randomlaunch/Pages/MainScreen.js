/**
 * Main Application Screen
 */

import React, { Component } from 'react';

import {
    StyleSheet,
    Text,
    View,
    AppState,
    PushNotificationIOS,
    Navigator,
    Image,
    ActivityIndicator
} from 'react-native';

import Launch from './LaunchScreen';
import About from './AboutScreen';
import Setting from './SettingScreen';
import RandomlaunchApiProvider from '../Libraries/RandomlaunchApi';

let RandomlaunchApi = new RandomlaunchApiProvider('ScV1h6zTV8J0jlW2Ef0cfLdn0pAJR1Td');

class MainScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            activeNav: 'launch'
            // activeNav: 'setting'
        };

        this._onNavigatorClicked = this._onNavigatorClicked.bind(this);
        this._maybeLogin = this._maybeLogin.bind(this);
        this._register = this._register.bind(this);
    }

    componentWillMount() {
        AppState.addEventListener('change', this._onAppStateChange);

        this._maybeLogin();
    }

    _maybeLogin() {
        if (! this.props.settingControl.getItem('authKey')) {
            return this._register();
        }

        RandomlaunchApi
            .authKey(this.props.settingControl.getItem('authKey'))
            .login()
            .then((response) => {
                if (response.status === 200) {
                    this.setState({isLoading: false});
                    return true;
                }
                if (response.status === 401) {
                    return this._register();
                }
            });
    }

    _register() {
        RandomlaunchApi.register()
            .then(response => response.json())
            .then(jsonData => {
                this.props.settingControl
                    .setItem('authKey', jsonData.data.auth_key);

                RandomlaunchApi
                    .authKey(this.props.settingControl.getItem('authKey'));

                this.setState({isLoading: false});
            });
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this._onAppStateChange);
    }

    _onAppStateChange(currentAppState) {
        if (currentAppState == 'active') {
            PushNotificationIOS.setApplicationIconBadgeNumber(0);
        }
    }

    _onNavigatorClicked(pageTo) {
        this.refs.launchNavigator.push({page: pageTo});
    }

    _renderScene(route, navigator) {
        let globalProps = {
            navigator,
            onNavigatorClicked: this._onNavigatorClicked,
            settingControl: this.props.settingControl,
            googleAnalytic: this.props.googleAnalytic,
            randomlaunchApi: RandomlaunchApi
        };

        switch (route.page) {
            case "about":
                // return <Text>About</Text>;
                return <About {...globalProps} />;
            break;

            case "launch":
                return <Launch {...globalProps} />;
            break;

            case "setting":
                return <Setting {...globalProps} />;
            break;
        }
    }

    render() {
        if (this.state.isLoading) {
            return (
                <Image source={require('image!background')} style={styles.loadingContainer} />
            )
        }

        return (
            <Image source={require('image!background')} style={styles.container}>

                <Navigator
                    ref="launchNavigator"
                    initialRoute={{page: this.state.activeNav}}
                    renderScene={this._renderScene.bind(this)}
                />
            </Image>
        );
    }
}

const styles = StyleSheet.create({
    loadingContainer: {
        flex: 1,
        width: null,
        height: null,
        justifyContent: "center",
        alignItems: 'center'
    },
    container: {
        flex: 1,
        width: null,
        height: null,
    },
});

module.exports = MainScreen;
