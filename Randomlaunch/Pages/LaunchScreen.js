/**
 * Launch Screen
 */

import React, { Component } from 'react';

import {
    StyleSheet,
    Text,
    View,
    AppState,
    PushNotificationIOS,
    TouchableOpacity,
    Dimensions,
    AsyncStorage,
    Image
} from 'react-native';

import LaunchScreenNavigation from '../Components/LaunchScreenNavigation';
import CountdownCircle from '../Components/CountdownCircle';
import LaunchButton from '../Components/LaunchButton';
// import RandomlaunchApiProvider from '../Libraries/RandomlaunchApi';
// import ScrollMenu from '../Components/ScrollMenu';

// let RandomlaunchApi = new RandomlaunchApiProvider('asdf', 'OmFzZGZhc2Rm');
const notificationMessasges = [
    "Woop! The next random link is ready :)",
    "Come check out the next random link ^^y",
    "Bored? Maybe the next random link will cheer you up!",
    "It is that time! The next random link is launched~",
    "Suprise! surprise! Next random link is ready!",
    "The next random link is now ready for ya!",
    "Wonder what the next random link is? Come check it out!",
];

class LaunchScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            countDown: -1,
            nextLaunchTime: null
        };

        this._onAppStateChange = this._onAppStateChange.bind(this);
        this._onSettingUpdated = this._onSettingUpdated.bind(this);
        this._countDownReachedZero = this._countDownReachedZero.bind(this);
    }

    componentWillMount() {
        AppState.addEventListener('change', this._onAppStateChange);
        this.props.settingControl.addEventListener('settingUpdated', this._onSettingUpdated);

        this._fetchCountDown();

        this.props.googleAnalytic.screenView('Main Screen');
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this._onAppStateChange);
        this.props.settingControl.removeEventListener('settingUpdated', this._onSettingUpdated);
    }

    componentWillUpdate(nextProps, nextStates) {
        console.log(
            this.state.nextLaunchTime,
            nextStates.nextLaunchTime,
            this.props.settingControl.getItem('notificationEnabled'),
            'checking for notification updates'
        );

        if (
            this.state.nextLaunchTime !== nextStates.nextLaunchTime
            && nextStates.nextLaunchTime
            && this.props.settingControl.getItem('notificationEnabled')
        ) {
            this._updateNotification(nextStates);
        }
    }

    _onAppStateChange(currentAppState) {
        if (currentAppState == 'active') {
            this._fetchCountDown();
        }

        if (currentAppState == 'background') {
            this.setState({countDown: -1});
        }
    }

    _onSettingUpdated(events) {
        let {key, value}= events;

        if (key === 'notificationEnabled') {
            if (value) {
                this._updateNotification(this.state);
            } else {
                console.log('cancleing all notificaaiton');
                PushNotificationIOS.cancelAllLocalNotifications();
            }
        }
    }

    _updateNotification(newStates) {
        console.log('canceling => ', this.state.nextLaunchTime);

        // if the app is reopened, we need to clearn the previous one as well
        if (this.state.nextLaunchTime === null) {
            PushNotificationIOS.cancelAllLocalNotifications();
        } else {
            PushNotificationIOS.cancelLocalNotifications({
                timestamp: this.state.nextLaunchTime
            });
        }

        console.log('adding => ', newStates.nextLaunchTime);
        PushNotificationIOS.scheduleLocalNotification({
            applicationIconBadgeNumber: 1,
            // fireDate: new Date(Date.now() + (5 * 1000)).toISOString(),
            fireDate: new Date(newStates.nextLaunchTime * 1000).toISOString(),
            alertBody: this._getRandomMessage(),
            userInfo: {
                timestamp: newStates.nextLaunchTime
            }
        });

        // for (let i = 0; i < 1; i ++) {
        //     PushNotificationIOS.scheduleLocalNotification({
        //         applicationIconBadgeNumber: 1,
        //         fireDate: new Date(Date.now() + (i * 5 * 1000)).toISOString(),
        //         alertBody: this._getRandomMessage()
        //     });
        // }
    }

    _getRandomMessage() {
        let index = Math.floor(Math.random() * (notificationMessasges.length));
        return notificationMessasges[index];
    }

    _fetchCountDown() {
        this.props.randomlaunchApi.getCountDown()
            // .then(response => console.log(response))
            .then(response => response.json())
            .then(jsonData => {
                this.setState({
                    countDown: jsonData.data.countDown,
                    nextLaunchTime: jsonData.data.nextTimestamp
                });
            }).done();
    }

    _getWebUrl() {
        // if (! this.selectedCategory) {
            return this.props.randomlaunchApi.getLaunchUrl();
        // }

        // let categoryName = this.state.categories[this.selectedCategory].name;
        // return RandomlaunchApi.getLaunchUrl(categoryName);
    }

    _countDownReachedZero() {
        this._fetchCountDown();
    }

    // _onCategorySelected(selectedCategory) {
    //     this.selectedCategory = selectedCategory;
    // }

    render() {
        return (
            <Image source={require('image!background')} style={styles.container}>
                    <LaunchScreenNavigation onNavigatorClicked={this.props.onNavigatorClicked} {...this.props}/>

                    <CountdownCircle
                        countDown={this.state.countDown}
                        countDownReachedZero={this._countDownReachedZero}
                    />

                    <LaunchButton webUrl={this._getWebUrl()} {...this.props} />

                    <Text style={styles.decorationText}>It is so random</Text>
                    <Text style={[styles.decorationText, styles.decortationEnd]}>we don't even know what it is!</Text>

                    {
                        // <ScrollMenu
                        //     items={this.state.categories}
                        //     selected={this.selectedCategory}
                        //     onItemSelected={this._onCategorySelected}
                        //     showPreText={false}
                        // />
                    }
            </Image>
        );
    }
}


const styles = StyleSheet.create({
    background: {
        flex: 1,
    },
    container: {
        // marginTop: 120,
        flex: 1,
        width: null,
        height: null,
        // justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#ecf0f1',
        padding: 25
    },
    decorationText: {
        marginTop: 20,
        color: '#c3c3c3',
        fontStyle: 'italic',
        fontWeight: '300',
        textAlign: 'center',
        fontSize: 12,
        lineHeight: 20
    },
    decortationEnd: {
        marginTop: 0
    }
});

module.exports = LaunchScreen;
