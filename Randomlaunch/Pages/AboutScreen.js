/**
 * About Screen
 */

import React, { Component } from 'react';

import {
    StyleSheet,
    Text,
    ScrollView,
    Image
} from 'react-native';

import FakeTopBar from '../Components/FakeTopBar';

class AboutScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {

        };
    }

    componentWillMount() {

        this.props.googleAnalytic.screenView('About Screen');
    }

    render() {
        return (
            <Image source={require('image!background')} style={styles.container}>
                <FakeTopBar {...this.props} title="About Us" backPosition="left" />

                <ScrollView style={styles.content}>
                    <Text style={styles.heading}>About RandomLaunch.</Text>
                    <Text style={styles.paragraph}>
                        We are just a bunch of Internet geeks who love the web, however; we got bored with the same websites everyday so we decided to create something random and unique. With over <Text style={styles.bold}>1 billion websites</Text> out there, we decided to generate some random websites just for kicks.
                    </Text>
                    <Text style={styles.paragraph}>
                        It all started with a simple conversation about Internet boredom then slowly evolved into wanting to see more of the web. Slowly from an idea and concept we built it into a fun hobby.
                    </Text>
                    <Text style={styles.paragraph}>
                        Hence born <Text style={styles.bold}>RandomLaunch!</Text> Who knows, maybe we will discover some random unknown gem we will all learn to love!
                    </Text>

                    <Text style={styles.heading}>How It Works?</Text>
                    <Text style={styles.paragraph}>
                        So, you have been using RandomLaunch for a while now and are curious to know how this all works and why the logic works the way it works. <Text style={styles.bold}>Shh... it is a secret!</Text>
                    </Text>
                    <Text style={styles.paragraph}>
                        For the most part, it is actually quite simple. We borrowed our grandmother's <Text style={styles.bold}>secret recipe</Text> and added additional ingredients and magical potions to spice up the scientific randomness, including a leprechaun's hat.
                    </Text>
                    <Text style={styles.paragraph}>
                        Then, we put it all inside a pot and boil for <Text style={styles.bold}>4 hours</Text> until it is ready to be sifted to remove unwanted materials.
                    </Text>
                    <Text style={styles.paragraph}>
                        And at last, voila, the course is ready to be served.
                    </Text>

                    <Text style={styles.heading}>Disclaimer</Text>
                    <Text style={styles.paragraph}>
                        We have taken out all the inappropriate, violent, and pornographic ingredients from our grandmother's kitchen. All the random links that's launched will be safe for all ages.
                    </Text>
                    <Text style={styles.paragraph}>
                        However, the site's content might be against your personal religion, believes, moral or political standpoint.
                    </Text>
                    <Text style={styles.paragraph}>
                        Therefore, by using our website and/or mobile applications, <Text style={styles.disclaimer}>you hereby agree that all links generated are entirely random and will not hold RandomLaunch, their affiliates, their providers, their staff, and pets liable for damages that may occur as a result of using this website or application.</Text>
                    </Text>
                    <Text style={styles.paragraph}>
                        Otherwise, we hope you enjoy using RandomLaunch!
                    </Text>

                    <Text style={styles.copyright}>
                        copyright © 2016 RandomLaunch.
                    </Text>
                    <Text style={styles.reserved}>
                        all rights reserved.
                    </Text>

                </ScrollView>
            </Image>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null
    },
    content: {
        paddingRight: 20,
        paddingLeft: 20
    },
    heading: {
        color: 'white',
        fontSize: 20,
        marginTop: 35,
        marginBottom: 15,
        letterSpacing: 1.5,
        fontWeight: '500',
    },
    paragraph: {
        color: 'white',
        marginBottom: 14,
        fontSize: 14,
        lineHeight: 26,
        fontWeight: '200'
    },
    copyright: {
        color: 'white',
        textAlign: 'center',
        marginTop: 35,
        fontSize: 11,
        lineHeight: 22,
    },
    reserved: {
        color: 'white',
        textAlign: 'center',
        marginBottom: 25,
        fontSize: 11,
        lineHeight: 22,
    },
    bold: {
        fontWeight: "bold"
    },
    disclaimer: {
        fontWeight: "bold",
        fontStyle: 'italic'
    },
    idunno: {
        fontSize: 11,
        fontWeight: '400'
    }
});

module.exports = AboutScreen;
