
import React, { Component } from 'react';

import {
    StyleSheet,
    Text,
    View,
    Image,
    AsyncStorage,
    TouchableOpacity,
    TouchableWithoutFeedback
} from 'react-native';

import * as Animatable from 'react-native-animatable';
import LaunchButton from './../../Components/LaunchButton';
import CountdownCircle from './../../Components/CountdownCircle';

class TutorialCountDown extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showSteps: 0,
            initFade: 500,
            paragraphFade: 1000
        };

        this._prevStep = this._prevStep.bind(this);
        this._nextStep = this._nextStep.bind(this);
        this._launchPressed = this._launchPressed.bind(this);
        this._countDownReachedZero = this._countDownReachedZero.bind(this);
    }

    componentWillMount() {
        this.props.settingControl.setItem('tutorialStep', 3);
    }

    _prevStep() {
        this.props.navigatorPrev(2);
    }

    _nextStep() {
        this.props.navigatorNext(4);
    }

    _launchPressed() {
        this.setState({showSteps: 1});
    }

    _countDownReachedZero() {
        this.setState({showSteps: 1});
    }

    render() {
        return (
            <Image source={require('image!background')} style={styles.container}>
                <Text style={styles.heading}>
                    <Animatable.Text animation="fadeIn" delay={this.state.initFade} duration={500} style={styles.hiddenText}>Next Link</Animatable.Text>
                    <Animatable.Text animation="fadeIn" delay={this.state.initFade + 500} duration={300} style={styles.hiddenText}>?</Animatable.Text>
                </Text>

                <Animatable.View animation="fadeIn" delay={this.state.initFade + 500} duration={500}>
                    <CountdownCircle
                        noListener={true}
                        countDown={5}
                        timeFrame={5}
                        circleSize={100}
                        circleWidth={3}
                        showNotes={false}
                        circleHolder={{width: 100, height: 100, marginBottom: 20}}
                        timerHolder={{width: 100, top: 40}}
                        timer={{fontSize: 20, color: '#c3c3c3'}}
                        countDownReachedZero={this._countDownReachedZero}
                    />
                </Animatable.View>

                <Animatable.Text animation="fadeInUp" delay={this.state.paragraphFade + 500} style={styles.paragraph}>
                    The countdown clock is displayed to show you when the next random web site will be released.
                </Animatable.Text>

                <Animatable.Text animation="fadeInUp" delay={this.state.paragraphFade + 1000} style={styles.paragraph}>
                    We have some crazy algorithmic rocket science calculations running in the background to generate the next random link, therefore, it will take some time before we can reveal the next web site.
                </Animatable.Text>

                <View style={{opacity: this.state.showSteps}}>
                    <TouchableOpacity onPress={this._nextStep}>
                        <Text style={styles.nextLink}>But... »</Text>
                    </TouchableOpacity>

                    {
                        // <TouchableOpacity onPress={this._prevStep}>
                        //     <Text style={styles.prevLink}>« back</Text>
                        // </TouchableOpacity>
                    }
                </View>
            </Image>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#ecf0f1',
        padding: 25,
        width: null,
        height: null
    },
    heading: {
        fontSize: 26,
        textAlign: 'center',
        color: '#c3c3c3',
        margin: 10,
        marginBottom: 25,
    },
    hiddenText: {
        opacity: 0,
    },
    paragraph: {
        opacity: 0,
        fontSize: 14,
        lineHeight: 22,
        textAlign: 'justify',
        color: '#c3c3c3',
        marginBottom: 15,
    },
    bold: {
        fontWeight: '700'
    },
    nextLink: {
        fontSize: 14,
        letterSpacing: 2,
        fontWeight: '500',
        color: '#dc1623',
        marginTop: 15
    },
    prevLink: {
        fontSize: 10,
        letterSpacing: 1.5,
        textAlign: "center",
        fontWeight: '500',
        color: '#c3c3c3',
        marginTop: 20
    }
});


module.exports = TutorialCountDown;
