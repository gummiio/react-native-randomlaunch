/**
 * Tutorial Main Screen
 */

import React, { Component } from 'react';

import {
    StyleSheet,
    Text,
    View,
    AsyncStorage,
    Navigator,
    Image
} from 'react-native';

import TutorialIntro from './TutorialIntro';
import TutorialButton from './TutorialButton';
import TutorialCountDown from './TutorialCountDown';
import TutorialSetting from './TutorialSetting';

class Tutorials extends Component {

    constructor(props) {
        super(props);

        this._renderScene = this._renderScene.bind(this);
        this._navigatorPrev = this._navigatorPrev.bind(this);
        this._navigatorNext = this._navigatorNext.bind(this);
    }

    componentWillMount() {
        this.props.googleAnalytic.screenView('Tutorial Screen');
    }

    _renderScene(route, navigator) {
        let navigatorPorps = {
            navigator,
            navigatorPrev: this._navigatorPrev,
            navigatorNext: this._navigatorNext,
            mainNavigator: this.props.mainNavigator,
            settingControl: this.props.settingControl,
            googleAnalytic: this.props.googleAnalytic,
        };

        switch (route.step) {
            case 1:
                return <TutorialIntro {...navigatorPorps} />;

            case 2:
                return <TutorialButton {...navigatorPorps} />;

            case 3:
                return <TutorialCountDown {...navigatorPorps} />;

            case 4:
                return <TutorialSetting {...navigatorPorps} />;

            case 5:
                return <TutorialEnd {...navigatorPorps} />;
        }
    }

    _navigatorPrev(toStep) {
        let stackExists = false;

        this.refs.tutorialNavigator.state.routeStack.map((stack) => {
            if (stack.step == toStep) {
                stackExists = true;
            }
        });

        if (stackExists) {
            this.refs.tutorialNavigator.jumpBack();
        } else {
            this.refs.tutorialNavigator.replacePrevious({step: toStep});
        }
    }

    _navigatorNext(toStep) {
        let stackExists = false;

        this.refs.tutorialNavigator.state.routeStack.map((stack) => {
            if (stack.step == toStep) {
                stackExists = true;
            }
        });

        if (stackExists) {
            this.refs.tutorialNavigator.jumpForward();
        } else {
            this.refs.tutorialNavigator.push({step: toStep});
        }
    }

    render() {
        return (
            <Navigator
                initialRoute={{step: this.props.step}}
                ref="tutorialNavigator"
                renderScene={this._renderScene}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null
    }
});

Tutorials.defaultProps = {
    step: 1
};

module.exports = Tutorials;
