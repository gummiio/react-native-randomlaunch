
import React, { Component } from 'react';

import {
    StyleSheet,
    Text,
    View,
    Image,
    AsyncStorage,
    TouchableOpacity
} from 'react-native';

import * as Animatable from 'react-native-animatable';

class TutorialIntro extends Component {

    constructor(props) {
        super(props);

        this.state = {
            initFade: 500,
            paragraphFade: 2000
        };

        this._nextStep = this._nextStep.bind(this);
    }

    componentWillMount() {
        this.props.settingControl.setItem('tutorialStep', 1);
    }

    _nextStep() {
        this.props.navigatorNext(2);
    }

    render() {
        return (
            <Image source={require('image!background')} style={styles.container}>
                <Text style={styles.heading}>
                    <Animatable.Text animation="fadeIn" delay={200} style={styles.hiddenText}>Random</Animatable.Text>
                    <Animatable.Text animation="fadeIn" delay={this.state.initFade + 200} duration={500} style={styles.hiddenText}>.</Animatable.Text>
                    <Animatable.Text animation="fadeIn" delay={this.state.initFade + 400} duration={500} style={styles.hiddenText}>.</Animatable.Text>
                    <Animatable.Text animation="fadeIn" delay={this.state.initFade + 600} duration={500} style={styles.hiddenText}>.</Animatable.Text>
                    <Animatable.Text animation="fadeIn" delay={this.state.initFade + 1500} duration={300} style={styles.hiddenText}> what</Animatable.Text>
                    <Animatable.Text animation="fadeIn" delay={this.state.initFade + 1700} duration={300} style={styles.hiddenText}>?</Animatable.Text>
                </Text>

                <Animatable.Text animation="fadeInUp" delay={this.state.paragraphFade + 500} style={styles.paragraph}>
                    The Internet changed our life enormously. It is becoming more and more important for nearly everybody as it is one of the newest and most forward-looking media.
                </Animatable.Text>

                <Animatable.Text animation="fadeInUp" delay={this.state.paragraphFade + 1000} style={styles.paragraph}>
                    However, with over <Text style={styles.bold}>1 billion websites</Text> out there, it's a shame that we might not even know or see most of them. So, we decided to create the <Text style={styles.bold}>RandomLaunch</Text> app and generate some random websites just for kicks.
                </Animatable.Text>

                <Animatable.Text animation="fadeInUp" delay={this.state.paragraphFade + 1500} style={styles.paragraph}>
                    It is so random that we don't even know what it is, so stay tuned!
                </Animatable.Text>

                <Animatable.View animation="fadeIn" delay={this.state.paragraphFade + 3000} duration={500}>
                    <TouchableOpacity onPress={this._nextStep}>
                        <Text style={styles.nextLink}>HOW IT WORKS »</Text>
                    </TouchableOpacity>
                </Animatable.View>
            </Image>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#ecf0f1',
        padding: 25,
        width: null,
        height: null
    },
    heading: {
        fontSize: 26,
        textAlign: 'center',
        color: '#c3c3c3',
        margin: 10,
        marginBottom: 25,
    },
    hiddenText: {
        opacity: 0,
    },
    paragraph: {
        opacity: 0,
        fontSize: 14,
        lineHeight: 22,
        textAlign: 'justify',
        color: '#c3c3c3',
        marginBottom: 15,
    },
    bold: {
        fontWeight: '700'
    },
    nextLink: {
        fontSize: 14,
        letterSpacing: 2,
        fontWeight: '500',
        color: '#dc1623',
        marginTop: 15
    }
});


module.exports = TutorialIntro;
