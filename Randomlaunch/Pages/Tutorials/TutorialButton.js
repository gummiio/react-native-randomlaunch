
import React, { Component } from 'react';

import {
    StyleSheet,
    Text,
    View,
    Image,
    AsyncStorage,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ActivityIndicator
} from 'react-native';

import * as Animatable from 'react-native-animatable';
import LaunchButton from './../../Components/LaunchButton';

class TutorialButton extends Component {

    constructor(props) {
        super(props);

        this.state = {
            waiting: false,
            showButton: true,
            showSteps: false,
            initFade: 500,
            paragraphFade: 1500,
            afterButtonText: ''
        };

        this._prevStep = this._prevStep.bind(this);
        this._nextStep = this._nextStep.bind(this);
        this._launchPressed = this._launchPressed.bind(this);
    }

    componentWillMount() {
        this.props.settingControl.setItem('tutorialStep', 2);
    }

    _prevStep() {
        this.props.navigatorPrev(1);
    }

    _nextStep() {
        this.props.navigatorNext(3);
    }

    _launchPressed() {
        this.setState({
            waiting: true,
            showButton: false
        });

        setTimeout(() => {
            this.setState({
                waiting: false,
                showSteps: true
            });
        }, 1000);
    }

    render() {
        return (
            <Image source={require('image!background')} style={styles.container}>
                <Text style={styles.heading}>
                    <Animatable.Text animation="fadeIn" delay={this.state.initFade} duration={500} style={styles.hiddenText}>The "Random Link"</Animatable.Text>
                    <Animatable.Text animation="fadeIn" delay={this.state.initFade + 500} duration={300} style={styles.hiddenText}>!</Animatable.Text>
                </Text>

                <Animatable.Text animation="fadeInUp" delay={this.state.paragraphFade + 500} style={styles.paragraph}>
                    So, curious to know how links are generated and why the logic works the way it works. <Text style={styles.bold}>Shh... it is a secret!</Text>
                </Animatable.Text>

                <Animatable.Text animation="fadeInUp" delay={this.state.paragraphFade + 1000} style={styles.paragraph}>
                    It is actually quite simple. We borrowed our grandmother's <Text style={styles.bold}>secret recipe</Text> and added additional ingredients and magical potions to spice up the scientific randomness, including a leprechaun's hat.
                </Animatable.Text>

                <Animatable.Text animation="fadeInUp" delay={this.state.paragraphFade + 1500} style={styles.paragraph}>
                    Just Press the Launch Button to peek at the random site.
                </Animatable.Text>

                {this.state.showButton? <Animatable.View animation="fadeIn" delay={this.state.paragraphFade + 3000} duration={500} style={styles.buttonWrapper}>
                    <LaunchButton callback={this._launchPressed}/>
                </Animatable.View> : null}

                {this.state.waiting? <View style={styles.activityIndicator}><ActivityIndicator animating={true} /></View> : null}

                {this.state.showSteps? <View>
                    <TouchableOpacity onPress={this._nextStep}>
                        <Text style={styles.nextLink}>When is new link available? »</Text>
                    </TouchableOpacity>

                    {
                        // <TouchableOpacity onPress={this._prevStep}>
                        //     <Text style={styles.prevLink}>« back</Text>
                        // </TouchableOpacity>
                    }
                </View> : null}
            </Image>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#ecf0f1',
        padding: 25,
        width: null,
        height: null
    },
    heading: {
        fontSize: 26,
        textAlign: 'center',
        color: '#c3c3c3',
        margin: 10,
        marginBottom: 25,
    },
    hiddenText: {
        opacity: 0,
    },
    paragraph: {
        opacity: 0,
        fontSize: 14,
        lineHeight: 22,
        textAlign: 'justify',
        color: '#c3c3c3',
        marginBottom: 15,
    },
    bold: {
        fontWeight: '700'
    },
    activityIndicator: {
        marginTop: 35
    },
    buttonWrapper: {
        position: 'relative'
    },
    nextLink: {
        fontSize: 14,
        letterSpacing: 2,
        fontWeight: '500',
        color: '#dc1623',
        marginTop: 15
    },
    prevLink: {
        fontSize: 10,
        letterSpacing: 1.5,
        textAlign: "center",
        fontWeight: '500',
        color: '#c3c3c3',
        marginTop: 30
    }
});


module.exports = TutorialButton;
