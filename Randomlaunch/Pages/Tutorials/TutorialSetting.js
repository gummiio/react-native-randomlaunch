
import React, { Component } from 'react';

import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Switch,
    PushNotificationIOS
} from 'react-native';

import * as Animatable from 'react-native-animatable';

class TutorialSetting extends Component {

    constructor(props) {
        super(props);

        this.state = {
            initFade: 500,
            paragraphFade: 500,
            notificationEnabled: props.settingControl.getItem('notificationEnabled')
        };

        this._nextStep = this._nextStep.bind(this);
        this._toggleNotificationSetting = this._toggleNotificationSetting.bind(this);
    }

    componentWillMount() {
        this.props.settingControl.setItem('tutorialStep', 4);
    }

    _toggleNotificationSetting(notificationEnabled) {
        this.props.settingControl.setItem('notificationEnabled', notificationEnabled);
        this.setState({notificationEnabled});

        if (notificationEnabled) {
            PushNotificationIOS.checkPermissions((permission) => {
                let {alert, badge, sound} = permission;

                if (! alert || ! badge || ! sound) {
                    PushNotificationIOS.requestPermissions();
                }
            });

            this.props.googleAnalytic.event('Switches', 'Switched', 'Initial Notification', 1);
        } else {
            this.props.googleAnalytic.event('Switches', 'Switched', 'Initial Notification', 0);
        }
    }

    _nextStep() {
        let tutorialFinished = true;
        this.props.settingControl.setItems({tutorialFinished});
        this.props.mainNavigator.push({tutorialFinished});
    }

    render() {
        return (
            <Image source={require('image!background')} style={styles.container}>
                <Text style={styles.heading}>
                    <Animatable.Text animation="fadeIn" delay={200} style={styles.hiddenText}>"How am I going to remember?"</Animatable.Text>
                </Text>

                <Animatable.Text animation="fadeInUp" delay={this.state.paragraphFade + 500} style={styles.paragraph}>
                    You can enable the push notification, if you wish to get notified when the next random link is launched.
                </Animatable.Text>

                <Animatable.View animation="fadeIn" delay={this.state.paragraphFade + 1500} style={styles.notificationHolder}>
                    <Text style={styles.notificationText}>
                        <Text>Enable Notification</Text>

                        <Switch
                            onValueChange={this._toggleNotificationSetting}
                            style={styles.notificationSwitch}
                            value={this.state.notificationEnabled}
                            onTintColor="#dc1623"
                            thumbTintColor="#c3c3c3"
                            tintColor="#c3c3c3"
                        />
                    </Text>
                </Animatable.View>

                <Animatable.View animation="fadeIn" delay={this.state.paragraphFade + 1500}>
                    <Text style={styles.notificationNotes}>(you can still enable this in the setting later)</Text>
                </Animatable.View>

                <Animatable.View animation="fadeIn" delay={this.state.paragraphFade + 2000} duration={500}>
                    <TouchableOpacity onPress={this._nextStep}>
                        <Text style={styles.nextLink}>Finish »</Text>
                    </TouchableOpacity>
                </Animatable.View>
            </Image>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#ecf0f1',
        padding: 25,
        width: null,
        height: null
    },
    heading: {
        fontSize: 26,
        textAlign: 'center',
        color: '#c3c3c3',
        margin: 10,
        marginBottom: 25,
    },
    hiddenText: {
        opacity: 0,
    },
    paragraph: {
        opacity: 0,
        fontSize: 14,
        lineHeight: 22,
        textAlign: 'justify',
        color: '#c3c3c3',
        marginBottom: 15,
    },
    bold: {
        fontWeight: '700'
    },
    nextLink: {
        fontSize: 14,
        letterSpacing: 2,
        fontWeight: '500',
        color: '#dc1623',
        marginTop: 15
    },
    notificationHolder: {
        marginTop: 30,
        marginBottom: 25
    },
    notificationText: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        color: '#c3c3c3',
        fontWeight: "600"
    },
    notificationSwitch: {
        marginTop: 10,
        marginLeft: 10
    },
    notificationNotes: {
        textAlign: "right",
        fontStyle: "italic",
        fontSize: 12,
        color: "#c3c3c3",
        marginBottom: 20
    }
});


module.exports = TutorialSetting;
